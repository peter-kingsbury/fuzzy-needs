@tool
extends CharacterBody2D


@onready var animation_player = $AnimationPlayer
@onready var animated_sprite = $AnimatedSprite2d


enum FacingEnum {
	up,
	down,
	left,
	right
}

@export var facing:FacingEnum = FacingEnum.down
@onready var fsm = $FuzzyStateMachine


func apply_environmental_changes():
	
	pass


func get_fsm():
	return fsm

