extends Node


signal do_action



@export var should_determine_actions:bool = true


@onready var needs = $Needs
@onready var conditions = $Conditions
@onready var actions = $Actions


@onready var enabled_needs
var last_desired_action = null


func _ready():
	enabled_needs = get_enabled_needs()
	
	for n in enabled_needs:
		n.connect("membership_changed", self._on_need_membership_changed)
	
	if should_determine_actions:
		determine_actions()


func _process(_delta):
	determine_actions()
	pass


func _on_need_membership_changed(_need):
	determine_actions()


func get_enabled_needs():
	return needs.get_children().filter(
		func(child):
			return child.is_enabled == true
	)


func determine_actions():
	for action in actions.get_children():
		if action.are_all_conditions_met():
			var action_name = action.name
			if action_name != last_desired_action:
				emit_signal("do_action", action_name)
				last_desired_action = action.name
			return


func get_all_conditions() -> Dictionary:
	var data = {}
	for condition in conditions.get_children():
		if condition.is_enabled and condition.need:
			var active = false
			if condition.is_met():
				active = true
			data[condition.name] = {
				"active": active,
				"value": condition.need.value
			}
	return data


func apply_needs(new_needs):
	for key in new_needs.keys():
		var value = new_needs[key]
		var need = needs.find_child(key)
		if key:
			need.value = clamp(need.value + value, 0., 1.)

