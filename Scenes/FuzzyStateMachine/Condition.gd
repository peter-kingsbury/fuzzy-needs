extends Node
class_name FuzzyCondition


@export var need:Node
@export var membership:Constants.MembershipEnum = Constants.MembershipEnum.low

@onready var last_check = Time.get_ticks_msec()
@export var check_frequency_ms = 1000

@export var is_enabled:bool = true


func get_membership():
	return membership


func is_met():
	return membership >= need.get_membership()
