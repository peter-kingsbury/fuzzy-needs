extends Node
class_name FuzzyAction


@export var is_enabled:bool = true
@export var conditions:Array[NodePath] = []


func are_all_conditions_met() -> bool:
	if not is_enabled:
		return false
	if conditions.size() == 0:
		return false
	for node_path in conditions:
		var condition = get_node(node_path)
		if not condition.is_met():
			return false
	return true


func do():
	print("Performing action %s ..." % [ name ])

