@tool
extends Node
class_name FuzzyNeed


signal membership_changed(need:Node)


@export var membership_curve:Curve
@export var is_enabled = true
@export var value:float = 0.0 : 
	set(val):
		value = val
		update_membership()
	get:
		return value


func _ready():
	init_curve()


func init_curve():
	membership_curve = Curve.new()
	membership_curve.min_value = 0.0
	membership_curve.max_value = 1.0
#	membership_curve.add_point(Vector2(0.0, 0.0))
	membership_curve.add_point(Vector2(0.3, 0.3))
	membership_curve.add_point(Vector2(0.6, 0.6))
#	membership_curve.add_point(Vector2(1.0, 1.0))
	


func update_membership():
	last_membership = membership
	membership = get_membership()
	if last_membership != membership:
		emit_signal("membership_changed", self)


func get_membership():
	if value <= 0.333:
		return Constants.MembershipEnum.low
	elif value <= 0.666:
		return Constants.MembershipEnum.medium
	else:
		return Constants.MembershipEnum.high


var membership = get_membership()
var last_membership = membership

