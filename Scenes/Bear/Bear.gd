extends "res://Scenes/Entity/Entity.gd"


const SPEED = 3
var step = 48
var movement:Vector2 = Vector2(0, 0)
var anim_direction = "right"
@onready var sleep_particles = $SleepParticles
@onready var last_position = position


func _ready():
	fsm.connect("do_action", self._on_do_action)


func _process(_delta):
	return
	move_and_collide(movement.normalized() * SPEED)
	
	if movement == Vector2.ZERO:
		# we are idle
		var animation_name = "still %s" % [ anim_direction ]
#		animated_sprite.play(animation_name)
		if animated_sprite.animation != animation_name:
			animated_sprite.play(animation_name)
#		if animation_player.get_current_animation() != animation_name:
#			animation_player.play(animation_name)
	else:
		var animation_name = "run %s" % [ anim_direction ]
		animated_sprite.play(animation_name)
#		if animation_player.get_current_animation() != animation_name:
#			animation_player.play(animation_name)


func _input(event):
	if event.is_action_pressed("move_up"):
		movement.y = -1
#		anim_direction = "left"
	elif event.is_action_released("move_up"):
		movement.y = 0
	
	if event.is_action_pressed("move_down"):
		movement.y = 1
#		anim_direction = "right"
	elif event.is_action_released("move_down"):
		movement.y = 0
	
	if event.is_action_pressed("move_right"):
		movement.x = 1
		anim_direction = "right"
	elif event.is_action_released("move_right"):
		movement.x = 0
	
	if event.is_action_pressed("move_left"):
		movement.x = -1
		anim_direction = "left"
	elif event.is_action_released("move_left"):
		movement.x = 0


func _on_game_tick_timeout():
	get_fsm().apply_needs({
		"Sleep": 0.01
	})


func sleep():
	var animation_name = "die %s" % [ anim_direction ]
	animated_sprite.play(animation_name)
	await animated_sprite.animation_finished
	animated_sprite.stop()
	animated_sprite.frame = animated_sprite.frames.get_frame_count(animation_name) - 1
	sleep_particles.show()


func wake_up():
	var animation_name = "die %s" % [ anim_direction ]
	animated_sprite.play(animation_name, true)


func _on_do_action(action):
	sleep()


