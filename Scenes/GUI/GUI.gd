extends Control


@onready var richtext = $Control/RichTextLabel
var context:Node


func _process(_delta):
	if not context:
		return
	
	var fsm = context.get_fsm()
	var conditions = fsm.get_all_conditions()
	
	richtext.text = ""
	
	for condition in conditions.keys():
		var data = conditions[condition]
		var color = "red"
		if data.active:
			color = "green"
		richtext.text += "%s (%0.f%%): [color=%s]%s[/color]\n" % [ condition, (data.value * 100), color, data.active ]


func set_context(ctx:Node):
	context = ctx

